﻿using System.Collections;
using UnityEngine;

using UnityEngine.SceneManagement;
//scary ready-made code
public class Game : MonoBehaviour
{
    [SerializeField] private StoryData _data;

    private TextDisplay _output; //stores place to output text
    private BeatData _currentBeat; //stores the data of the current story beat
    private WaitForSeconds _wait;

    [SerializeField]
    private EnvironmentController _environmentController; //reference to environment controller

    private int _lastBeatID; //beat ID of last thing displayed

    [SerializeField]
    private Animator fadeOut;

    public enum displayState 
    {
        clear,
        display
    }

    displayState _displayState;

    private void Awake()
    {
        _output = GetComponentInChildren<TextDisplay>(); //gets TextDisplay component and stores as _output
        _currentBeat = null; //no current beat is selected, so set to null
        _wait = new WaitForSeconds(0.5f); //assigns ""wait"" as 0.5 seconds (used later)
        _lastBeatID = 1;
        _displayState = displayState.clear;

        Time.timeScale = 1;
    }

    private void Update()
    {
        if(_displayState == displayState.display)
        { 
            if(_output.IsIdle) 
            {
                if (_currentBeat == null)
                {
                    DisplayBeat(1);
                }
                else
                {
                    UpdateInput();
                }
            }
        }


    }

    public void Begin(int beatID)
    {
        _displayState = displayState.display;
        _output.clearCompletely();

        if(beatID == 0)
        {
            DisplayBeat(_lastBeatID);
        }
        else
        {
            DisplayBeat(beatID);
        }
        
    }

    public void Stop()
    {
        _displayState = displayState.clear;

        _output.Clear();
        _output.StopAllCoroutines();
    }

    private void UpdateInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(_currentBeat != null)
            {
                if (_currentBeat.ID == 1)
                {
                    Application.Quit();
                }
                else
                {
                    DisplayBeat(_currentBeat.ID);
                }
            }
        }
        else
        {
            KeyCode alpha = KeyCode.Alpha1;
            KeyCode keypad = KeyCode.Keypad1;

            for (int count = 0; count < _currentBeat.Decision.Count; ++count)
            {
                if (alpha <= KeyCode.Alpha9 && keypad <= KeyCode.Keypad9)
                {
                    if (Input.GetKeyDown(alpha) || Input.GetKeyDown(keypad))
                    {
                        ChoiceData choice = _currentBeat.Decision[count];
                        DisplayBeat(choice.NextID);
                        _lastBeatID = choice.NextID;
                        break;
                    }
                }

                ++alpha;
                ++keypad;
            }
            

        }
    }

    private void DisplayBeat(int id)
    {
        BeatData data = _data.GetBeatById(id);
        StartCoroutine(DoDisplay(data));
        _currentBeat = data;

        _environmentController.updateCurrentBeat(id);

    }

    private IEnumerator DoDisplay(BeatData data)
    {
        _output.Clear();

        while (_output.IsBusy)
        {
            yield return null;
        }

        _output.Display(data.DisplayText);

        while(_output.IsBusy)
        {
            yield return null;
        }
        
        for (int count = 0; count < data.Decision.Count; ++count)
        {
            ChoiceData choice = data.Decision[count];
            _output.Display(string.Format("{0}: {1}", (count + 1), choice.DisplayText));

            while (_output.IsBusy)
            {
                yield return null;
            }
        }

        if(data.Decision.Count > 0)
        {
            _output.ShowWaitingForInput();
        }
    }

    //HUGO'S ADDITIONS


    public void startFadeOut(float timeDelay)
    {
        //begins fade co-routine (can be called from inspector)
        StartCoroutine("fade", timeDelay);
    }

    //waits time specified, begins fade out animation, loads credits
    private IEnumerator fade(float time)
    {
        yield return new WaitForSeconds(time);

        fadeOut.SetBool("playFadeOut", true);
        yield return new WaitForSeconds(2.5f);

        SceneManager.LoadScene("CreditsScene");
    }
}
