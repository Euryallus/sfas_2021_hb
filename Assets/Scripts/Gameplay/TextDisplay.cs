﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using TMPro;

public class TextDisplay : MonoBehaviour
{
    public enum State { Initialising, Idle, Busy } //states display can be in (setup, idle or "writing to screen")

    [SerializeField]
    private List<NPCData> _npcData;

    private Color _drawColour;

    private TMP_Text _displayText; //text mesh pro component
    private string _displayString; //string displayed / to be displayed to said TMP component
    private WaitForSeconds _shortWait; 
    private WaitForSeconds _longWait;

    private WaitForSeconds _deleteWait;
    private State _state = State.Initialising; //sets current state to initialising as default

    private AudioManager _audioManager;

    public bool IsIdle { get { return _state == State.Idle; } } //returns true / false value of "idle" (if state == idle, return true, else false)
    public bool IsBusy { get { return _state != State.Idle; } } //returns true / false value (if state != idle, return true, else false)

    private void Awake()
    {
        _displayText = GetComponent<TMP_Text>(); //sets _displayText to TMP_Text component on gameObject
        _shortWait = new WaitForSeconds(0.025f); //assins _shortWait to 0.1 seconds
        _longWait = new WaitForSeconds(0.4f); //assigns _longWait to 0.4 seconds

        _deleteWait = new WaitForSeconds(0.05f);

        _displayText.text = string.Empty; //sets text display to empty
        _state = State.Idle; //sets state to Idle when initialisation is done

        SetSpeaker(0);

        _audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
    }

    private IEnumerator DoShowText(string text) //takes string to display as param
    {
        int currentLetter = 0; //pointer as to which letter is next to be displayed
        char[] charArray = text.ToCharArray(); //stores text to display as a character array

        while (currentLetter < charArray.Length) //cycles each character in the array while the pointer hasn't reached the end yet
        {
            _displayText.text += charArray[currentLetter++]; //adds the next character to display to the string component in _displayText

            if(currentLetter % 2 == 0)
            {
                _audioManager.Play("KeyPress", 0.7f, 1.2f);

            }
            yield return _shortWait; //waits 0.1 seconds before looping to the start of the while statement
        }

        _displayText.text += "\n"; //when text to display if fully on screen, add an ENTER (new line)
        _displayString = _displayText.text; // save _displayString to the text on screen (inc. \n)
        _state = State.Idle; //set state to idle again
    }

    private IEnumerator DoAwaitingInput() 
    {
        bool on = true; //sets local bool "on" to true

        while (enabled) //while behaviour is "enabled
        {
            _displayText.text = string.Format( "{0}> {1}", _displayString, ( on ? "|" : " " )); //creates blinking cursor symbol at input line, alternating each 0.8 seconds
            on = !on;
            yield return _longWait;
        }
    }

    private IEnumerator DoClearText() // cycles displayed text, emulates each line being deleted on an old terminal
    {
        int currentLetter = 0; 
        char[] charArray = _displayText.text.ToCharArray();

        while (currentLetter < charArray.Length)
        {
            
            //if (currentLetter > 0 && charArray[currentLetter -1] != '\n')
            //{
                   
            //}
        
            //if (charArray[currentLetter] != '\n')
            //{
                //charArray[currentLetter] = '_';
            //}

            if(charArray[currentLetter] == '\n')
            {
                for (int i = 0; i < currentLetter; i++)
                {
                    charArray[i] = ' ';
                    
                }

                if(currentLetter + 1 != charArray.Length)
                {
                    charArray[currentLetter + 1] = '_';
                }
                
                _displayText.text = charArray.ArrayToString();
                yield return _deleteWait;
            }
            ++currentLetter;
            
        }

        _displayString = string.Empty;
        _displayText.text = " ";

        StopAllCoroutines();
        _state = State.Idle;
    }

    public void Display(string text) //if the terminal is not busy, stop all coroutines and start displaying text passed
    {
        if (_state == State.Idle) 
        {
            StopAllCoroutines();
            _state = State.Busy;
            StartCoroutine(DoShowText(text));
        }
    }

    public void ShowWaitingForInput() //when state reaches idle, stop coroutines and start waiting for input
    {
        if (_state == State.Idle)
        {
            StopAllCoroutines();
            StartCoroutine(DoAwaitingInput());
        }
    }

    public void Clear() //when state is idle, stop coroutines and clear the screen
    {
        //if (_state == State.Idle)
        //{
        _displayString = "";
          StopAllCoroutines();
          _state = State.Busy;
          StartCoroutine(DoClearText());
       //}
    }

    public void SetSpeaker(/*NPCData.Characters characterToSwitchTo*/ int characterID) //UnityEvent doesnt allow normal parameter input, balls
    {
        _drawColour = _npcData[characterID].characterColour;

        _displayText.color = _npcData[characterID].characterColour;

        _shortWait = new WaitForSeconds(_npcData[characterID].talkSpeed);


    }

    public void clearCompletely()
    {
        _displayString = string.Empty;
        _displayText.text = " ";

        StopAllCoroutines();
        _state = State.Idle;
    }
}
