﻿using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class StoryData : ScriptableObject
{
    [SerializeField] private List<BeatData> _beats; //list of all possible story beats
 
    public BeatData GetBeatById( int id ) //finds a beat by it's ID 
    {
        return _beats.Find(b => b.ID == id); //returns beat with ID passed as param
    }

#if UNITY_EDITOR //if in the editor 
    public const string PathToAsset = "Assets/Data/Story.asset"; //creates path to Story asset

    public static StoryData LoadData() //loads data from said story asset
    {
        StoryData data = AssetDatabase.LoadAssetAtPath<StoryData>(PathToAsset);
        if (data == null) //if data does not exist, create a new instance of StoryData
        {
            data = CreateInstance<StoryData>();
            AssetDatabase.CreateAsset(data, PathToAsset); //create an asset of type Story Data in path to story asset (basically if dev deletes story data / it disappears it creates a new, empty ""story"")
        }

        return data; //returns this new instance of story
    }
#endif
}

