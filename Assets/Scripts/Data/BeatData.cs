﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BeatData
{

    [SerializeField] private List<ChoiceData> _choices; //lists choices available to choose from the beat
    [SerializeField] private string _text; //text component of beat stored as _text
    [SerializeField] private int _id;

    public List<ChoiceData> Decision { get { return _choices; } } //returns list of available choices
    public string DisplayText { get { return _text; } } //returns text associated with beat
    public int ID { get { return _id; } } //returns beat ID

}
