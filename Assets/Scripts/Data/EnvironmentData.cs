﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//used by the Environment controller
public class EnvironmentData : MonoBehaviour
{
    public int beatExecutedOn; //stores beat event should be executed on
    public UnityEvent beatEvents; //unityEvent data
}
