﻿using System;
using UnityEngine;

[Serializable]
public class ChoiceData
{
    [SerializeField] private string _text; //Text associated with choice
    [SerializeField] private int _beatId; //ID of beat choice "stems" from

    public string DisplayText { get { return _text; } } //returns text element of choice
    public int NextID { get { return _beatId; } } //returns ID of beat associated
}
