﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    //possible states the door can be in - either moving (opening or closing), or stationary (open or closed)
    enum state
    {
        closed,
        open,
        opening,
        closing
    }

    //saves gameobject associated with the "moving" part of the door, e.g. model w/ collider attached 
    [SerializeField]
    GameObject doorCollider;

    //open & closed positions to reference back to (openPos taken from empty game object @ the desired "open" position)
    Vector3 closedPos;
    [SerializeField]
    Transform openPos;

    //saves current state of door
    state currentState;

    private AudioManager _audioManager;

    void Start()
    {
        //sets state to closed as a default
        currentState = state.closed; 

        //saves "closed" pos as current position
        closedPos = doorCollider.transform.position;

        _audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
    }

    private void Update()
    {
        switch(currentState)
        {
            case state.opening:

                //if the door is not yet at its open position, lerp towards it
               doorCollider.transform.position = Vector3.Lerp(doorCollider.transform.position, openPos.position, 1 * Time.deltaTime);

                //if the distance between the current pos & the destination is small enough, just set the position to the destination
                if(Vector3.Distance(doorCollider.transform.position, openPos.position) < 0.001f)
                {
                    doorCollider.transform.position = openPos.position;
                    //flags that the door is now open and stationary
                    currentState = state.open;
                }
               break;

            case state.closing:
                //if the door's not fully closed yet, lerp towards closed position
                doorCollider.transform.position = Vector3.Lerp(doorCollider.transform.position, closedPos, 1 * Time.deltaTime);

                //if distance is small enough, just set the position to the desination
                if (Vector3.Distance(doorCollider.transform.position, closedPos) < 0.001f)
                {
                    doorCollider.transform.position = closedPos;
                    //flags that the door is now closed & stationary
                    currentState = state.closed;
                }
                break;
        }
    }

    public void open() //sets state to "opening"
    {
        currentState = state.opening;
        //_audioManager.Play("Door");


    }

    public void close() //sets state to "closing"
    {
        currentState = state.closing;
    }

    public void interact() //used with buttons, etc. 
    {
        //will cause the door to transition to the opposite state (e.g. if the door is open, close it)
        if(currentState == state.closed || currentState == state.closing)
        {
            currentState = state.opening;
        }
        else
        {
            currentState = state.closing;
        }
        
        _audioManager.Play("Door", 0.8f, 1.2f);
    }
}
