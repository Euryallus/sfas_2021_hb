﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Note data", menuName = "Note data/New note", order = 2)]
//stores text data associated with a "note" or log found
public class NoteData : ScriptableObject
{
    //stores title string
    [SerializeField]
    private string _title;

    //stores description string, resizes inspector input box to be big enough to write longer description
    [SerializeField]
    [TextArea(15,20)]
    private string _contents;

    //getters / setters for title & contents
    public string title { get { return _title; } }
    public string contents { get { return _contents; } }

}
