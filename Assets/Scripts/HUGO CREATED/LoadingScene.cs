﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour
{
    
    void Start()
    {
        StartCoroutine("LoadAsynchOperation");
    }

    IEnumerator LoadAsynchOperation()
    {
        //begins loading main level
        AsyncOperation mainLevel = SceneManager.LoadSceneAsync("MainScene");

        yield return new WaitForSeconds(1.5f);
    }
}
