﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    [SerializeField]
    private GameObject optionsBackground;

    [SerializeField]
    private Slider volSlider;
    [SerializeField]
    private Slider mouseSens;
    [SerializeField]
    private GameObject backButton;

    [SerializeField]
    private AudioSource backgroundAudio;

    private void Start()
    {
        optionsBackground.SetActive(false);
        volSlider.value = PlayerPrefs.GetFloat("Volume", 1f);
        mouseSens.value = PlayerPrefs.GetFloat("MouseSensitivity", 150f);

        volSlider.gameObject.transform.parent.gameObject.SetActive(false);
        mouseSens.gameObject.transform.parent.gameObject.SetActive(false);
        backButton.SetActive(false);
    }
    public void LoadMainLevel()
    {
        SceneManager.LoadScene("LoadingScene");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void setVolume()
    {
        PlayerPrefs.SetFloat("Volume", volSlider.value);
        backgroundAudio.volume = volSlider.value;
    }

    public void setMouseSens()
    {
        PlayerPrefs.SetFloat("MouseSensitivity", mouseSens.value);
    }

}
