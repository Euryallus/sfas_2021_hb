﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Audio;

[System.Serializable]
public class Sound
{
    public AudioClip _clip;
    public string _name;
    public float _volume;
    public float _pitch;

    public AudioSource _source;

}
