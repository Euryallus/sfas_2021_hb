﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class Button : MonoBehaviour
{
    [SerializeField]
    public UnityEvent buttonDownEvent; //Unity Event object (assigned in inspector) executed when button is initially pressed

    [SerializeField]
    public UnityEvent buttonUpEvent; //events to execute when button is lifted (e.g. close door)

    private bool _isPressed = false; //bool value, stores if button is pressed (true) or not (false)
    public bool pressed { get { return _isPressed; } }

    private void Start() //assigns UnityEvent to default event (will slip up if the event = null, avoids errors)
    {
        if(buttonDownEvent == null)
        {
            buttonDownEvent = new UnityEvent();
        }
        if(buttonUpEvent == null)
        {
            buttonUpEvent = new UnityEvent();
        }
    }

    private void OnTriggerEnter(Collider other) //when object (button pad) enters trigger volume (bottom of button)
    {
        GameObject _triggerObj = other.transform.gameObject; //grab collision game object

        if(_triggerObj.CompareTag("ButtonPad"))
        {
            buttonDownEvent.Invoke(); //runs all methods in buttonDownEvent event
            _isPressed = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        buttonUpEvent.Invoke(); //when button is released & button pad leaves trigger vol. execute all methods in buttonUpEvent event
        _isPressed = false;
    }
}
