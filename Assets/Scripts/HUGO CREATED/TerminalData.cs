﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//stores data about specific terminals
public class TerminalData : MonoBehaviour
{
    //allows dev. to alter which beat plays out when player interacts w/ terminal
    [SerializeField]
    private int _BeatToTrigger;

    //stores position camera should transition to upon "use"
    [SerializeField]
    private Transform _cameraFocalPoint;

    //stores whether terminal has begun use or not
    bool hasStarted;

    //stores whether terminal can be used or not
    [SerializeField]
    private bool _interactable;

    //getters / setters
    public bool interactable { get { return _interactable; }  set { _interactable = value; } }
    public int setBeatToTrigger { set { _BeatToTrigger = value; hasStarted = false; } }


    private void Start()
    {
        //flags all "conversations" as not started 
        hasStarted = false;
    }

    public int returnBeat()
    {
        if(hasStarted) //if the conversation is in full swing, return 0 (prompts Game to continue from last known beat ID)
        {
            return 0;
        }
        else //if conversation hasn't yet been started, begin convo & return beat to begin from
        {
            hasStarted = true;
            return _BeatToTrigger;
            
        }
    }

    public Transform returnCameraPos() //returns position of camera in "focused" mode
    {
        return _cameraFocalPoint;
    }


}
