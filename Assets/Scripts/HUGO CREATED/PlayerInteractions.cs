﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class PlayerInteractions : MonoBehaviour
{
    //attached to camera
    private RaycastHit _hit;
    [SerializeField]
    private Game screen;

    [SerializeField]
    private CameraController _camController;
    [SerializeField]
    private GameObject _transitionCamera;

    [SerializeField]
    private GameObject _heldObject;
    [SerializeField]
    private Transform playerHand;

    [SerializeField]
    private NoteMenu _noteMenu;

    [SerializeField]
    private Color interactPrompt;
    [SerializeField]
    private Color interactDefault;
    [SerializeField]
    private Image crosshair;

    [SerializeField]
    private AudioManager _audioManager;

    // Start is called before the first frame update
    void Start()
    {
        _transitionCamera.SetActive(false);
        crosshair.color = interactDefault;

        _audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckInteraction();
    }

    private void CheckInteraction()
    {
        
        Debug.DrawRay(transform.position, transform.forward * 2.5f);

        if (Physics.Raycast(transform.position, transform.forward, out _hit, 2.5f) && (_hit.transform.CompareTag("Interactable") || _hit.transform.CompareTag("Laptop") || _hit.transform.CompareTag("Note")))
        {
            crosshair.color = interactPrompt;
        }
        else
        {
            crosshair.color = interactDefault;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            GameObject _hitObj = null;
            if(Physics.Raycast(transform.position, transform.forward, out _hit, 2.5f))
            {
                _hitObj = _hit.transform.gameObject;
            }

            if(_hitObj != null)
            {
                //////////////////////////////////////////////////////////////
                if(_hitObj.CompareTag("Interactable") && _hitObj.gameObject.GetComponent<Interactable>())
                {
                    _hitObj.GetComponent<Interactable>().interact();
                }
                //////////////////////////////////////////////////////////////

                if (_hitObj.CompareTag("Laptop") && _hitObj.GetComponent<TerminalData>().interactable == true)
                {
                    int beat = _hit.transform.gameObject.GetComponent<TerminalData>().returnBeat();
                
                    screen.Begin(beat);

                    _transitionCamera.SetActive(true);
                    _camController._playerToFocus(transform, _hit.transform.gameObject.GetComponent<TerminalData>().returnCameraPos());

                    gameObject.SetActive(false);
                }

                if(_hitObj.CompareTag("Note"))
                {
                    _noteMenu.addNote(_hitObj.GetComponent<NoteItemScript>().noteData);

                    _hitObj.SetActive(false);
                    _audioManager.Play("Paper", 0.8f, 1f);
                }
            
            }

             if(_heldObject != null)
             {
                 _heldObject.GetComponent<movableObject>().dropObject(_heldObject.transform.position - transform.position);
                 _heldObject = null;
             }

             else if(_hitObj!= null && _hitObj.GetComponent<movableObject>() && _heldObject == null)
             {
                 _heldObject = _hit.transform.gameObject;
                 _heldObject.GetComponent<movableObject>().pickUp(playerHand);
             }
 
        }

        if(Input.GetMouseButtonDown(0) && _heldObject != null)
        {
            _heldObject.GetComponent<movableObject>().throwObject(_heldObject.transform.position - transform.position);
            _heldObject = null;
        }
    }



}
