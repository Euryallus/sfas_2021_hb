﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private Sound[] sounds;


    void Start()
    {
        //checks all "sounds" stored, if they don't have a source set self to be source
        for (int i = 0; i < sounds.Length; i++)
        {
            if(sounds[i]._source == null)
            {
                sounds[i]._source = gameObject.AddComponent<AudioSource>();
            }

            //sets pitch, vol etc. in accordance with sound data stored
            sounds[i]._source.clip = sounds[i]._clip;
            sounds[i]._source.volume = sounds[i]._volume * PlayerPrefs.GetFloat("Volume", 1.0f);
            sounds[i]._source.pitch = sounds[i]._pitch;
        }
    }



    //allows single param play command (from inspector / UnityEvent)
    public void environmnentPlay(string soundName)
    {
        foreach (Sound clip in sounds)
        {
            if (clip._name == soundName)
            {
                clip._source.Play();
            }
        }
    }

    //allows Play with varied pitch
    public void Play(string soundName, float pitchmax = 1, float pitchmin = 1)
    {
        foreach(Sound clip in sounds)
        {
            if(clip._name == soundName)
            {
                if(pitchmax != 1 && pitchmax != 1)
                {
                    clip._pitch = Random.Range(pitchmin, pitchmax);
                }

                clip._source.pitch = clip._pitch;
                clip._source.Play();
            }
        }
    }

    //stops sound passed from playing
    public void Stop(string soundName)
    {
        foreach (Sound clip in sounds)
        {
            if (clip._name == soundName)
            {
                clip._source.Stop();
            }
        }
    }
}
