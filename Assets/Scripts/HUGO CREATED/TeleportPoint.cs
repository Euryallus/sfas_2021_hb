﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPoint : MonoBehaviour
{
    [SerializeField]
    private Transform ownTransform;
    [SerializeField]
    private TeleportPoint destination;

    private float timeSinceLastTp = 0;

    [SerializeField]
    private float yRotation = 0;

    [SerializeField]
    public bool active = true;
    
    void Update()
    {
        timeSinceLastTp += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("P I N G");
        if(other.CompareTag("Player") && timeSinceLastTp > 1 && active)
        {
            destination.Teleport(other.gameObject);
            timeSinceLastTp = 0;
        }

    }

    public void Teleport(GameObject collisionObject)
    {
        if(timeSinceLastTp > 2)
        {
            collisionObject.GetComponent<CharacterController>().enabled = false;

            Vector3 offset = collisionObject.transform.position - destination.ownTransform.position;

            if(yRotation == 180)
            {
                offset = new Vector3(-offset.x, offset.y, -offset.z);
            }

            collisionObject.transform.position = ownTransform.position + offset;

            collisionObject.transform.Rotate(new Vector3(0, yRotation, 0), Space.Self);

            collisionObject.GetComponent<CharacterController>().enabled = true;


            timeSinceLastTp = 0;
        }

    }
}
