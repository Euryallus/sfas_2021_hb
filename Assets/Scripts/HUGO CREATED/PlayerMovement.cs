﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //stores X & Y mouse input after time scaling & sensitivity adjustments
    private float mouseX;
    private float mouseY;

    //raw mouse input data
    private float mouseInputX;
    private float mouseInputY;

     //stores on-going value of Y rotation (so mouse movement doesn't just reset it)
    private float rotateY;

     //raw button input (X and Y axis, adjustable in preferences)
    private float inputX;
    private float inputY;

    //player movement data, e.g. walk speed
    [SerializeField]
    private float playerSpeed = 20f;
    [SerializeField]
    private float playerRunSpeed = 30f;
    [SerializeField]
    private float yVelocity = -9.81f;
    [SerializeField]
    private float mouseSensitivity = 150f;

    //saves ref. to camera animations
    [SerializeField]
    private Animator cameraAnimator;
    
    //ref to characterController component (allows movement)
    [SerializeField]
    private CharacterController _controller;

    //ref to player camera
    [SerializeField]
    private Camera playerCamera;

    private AudioManager _audioManager;
    //possible movement states
    public enum movementState
    {
        idle,
        walking,
        crouching,
        sprinting
    }

    //stores current active state
    private movementState currentMovement;

    void Start()
    {
        //sets cursor state to "locked to middle of screen", invisible, & movement state to idle
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        currentMovement = movementState.idle;

        _audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();

        mouseSensitivity = PlayerPrefs.GetFloat("MouseSensitivity", 150f);
    }

    
    void Update() 
    {
        //takes raw input each update & processes in followiung functions
        inputX = Input.GetAxis("Horizontal");
        inputY = Input.GetAxis("Vertical");

        mouseInputX = Input.GetAxis("Mouse X");
        mouseInputY = Input.GetAxis("Mouse Y");

        playerInput();
        moveCamera();
        movePlayer();

    }

    void playerInput()
    {
        //checks current state & other key inputs (e.g. shift is down)

        //if shift is down, transition to sprinting
        if(Input.GetKeyDown(KeyCode.LeftShift) && currentMovement == movementState.walking)
        {
            currentMovement = movementState.sprinting;

            cameraAnimator.SetBool("isWalking", true);
            cameraAnimator.speed = playerRunSpeed / playerSpeed;

            float speed = 0.66f / (playerRunSpeed / playerSpeed);

            StopCoroutine("footsteps");
            StartCoroutine("footsteps", speed );
        }

        //if shift is up, transition to wlaking
        if(Input.GetKeyUp(KeyCode.LeftShift) && currentMovement == movementState.sprinting)
        {
            currentMovement = movementState.walking;
            cameraAnimator.SetBool("isWalking", true);
            cameraAnimator.speed = 1;
            StopCoroutine("footsteps");
            StartCoroutine("footsteps", 0.66f );
        }
    }

    void moveCamera()
    {
        //
        // ## CONTROLS CAMERA ROTATION
        // ## Takes mouse X and Y input and translates into rotation (Y clamped between -75 and 75)
        // ## Camera rotation affected by Y input, player rotation affected by X input
        // 
        // ## VIDEO LOOSLY FOLLOWED: https://youtu.be/_QajrabyTJc 2019 - Brackeys "FIRST PERSON MOVEMENT in Unity - FPS Controller"
        //

        mouseX = mouseInputX * mouseSensitivity * Time.deltaTime;
        mouseY = mouseInputY * mouseSensitivity * Time.deltaTime;

        mouseX = Mathf.Clamp(mouseX, -1f * mouseSensitivity * Time.deltaTime, mouseSensitivity * Time.deltaTime);
        mouseY = Mathf.Clamp(mouseY, -1f * mouseSensitivity * Time.deltaTime, mouseSensitivity * Time.deltaTime);

        rotateY -= mouseY; //inverts Y mouse input

        //clamps camera movement to a 150 degrees vertically
        rotateY = Mathf.Clamp(rotateY, -75f, 75f);
        

        //sets camera rotation to "processed" input
        playerCamera.transform.localRotation = Quaternion.Euler(rotateY , 0f, 0f);

        mouseX = Mathf.Clamp(mouseX, -1f * mouseSensitivity * Time.deltaTime, mouseSensitivity * Time.deltaTime);

        //rotates actual player gameobject based on X input
        transform.Rotate(Vector3.up * mouseX);
    }

    void movePlayer() 
    {
        
        float speed;

        //changes speed depending on movement state
        switch(currentMovement) 
        {
            case movementState.walking:
                speed = playerSpeed;
                break;

            case movementState.sprinting:
                speed = playerRunSpeed;
                break;

            case movementState.crouching:
                speed = playerSpeed / 1.5f;
                break;

            default:
                speed = playerSpeed;
                break;
        }

        //if the player has input SOMETHING movement-wise
        if(inputX != 0 || inputY != 0)
        {
            if(currentMovement == movementState.idle)
            { 
                //if the player is just starting to walk, change animation & change state
                cameraAnimator.SetBool("isWalking", true);
                currentMovement = movementState.walking;
                StartCoroutine("footsteps", 0.66f);
                cameraAnimator.speed = 1;
            }

            //creates new vector the player should move towards
            Vector3 moveTo = transform.right * inputX + transform.forward * inputY;
            //adjust Y velocity with grav.
            moveTo.y = yVelocity;

            //apply movement
            _controller.Move(moveTo * speed * Time.deltaTime);
        }
        else
        {
            //if the player hasnt moved this update, set the state and animation to idle
            if(currentMovement != movementState.idle)
            {
                currentMovement = movementState.idle;
                cameraAnimator.SetBool("isWalking", false);
                StopCoroutine("footsteps");
                _audioManager.Play("Footstep");
            }

        }

    }

    public void openMenu()
    {
        currentMovement = movementState.idle;
        cameraAnimator.SetBool("isWalking", false);
    }

    private IEnumerator footsteps(float rate)
    {
        while(true)
        {
            yield return new WaitForSeconds(rate);

            _audioManager.Play("Footstep", 0.8f, 1.2f);
        }
    }

}
