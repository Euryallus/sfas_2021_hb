﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Does what it says on the tin - controls the environment
public class EnvironmentController : MonoBehaviour
{
    //stores list of EnvironmentData (what events will occour when the story hits X beat)
    [SerializeField]
    EnvironmentData[] beatData;

    //specific to Oxygen task until I optimiste it / give up
    //stores list of buttons needed to complete the O2 task
    [SerializeField]
    List<Button> O2Buttons;
    //stores list of positions the buttons must be in to succeed (0 = Yellow, 1 = Green, 2 = Blue, 3 = Red, 4 = White)
    [SerializeField]
    bool[] downVsUp;

    [SerializeField] //stores Door component of the door leading from O2 puzzle
    private UnityEvent O2PuzzleComplete;

    bool solved = false;


    private void Start()
    {
        beatData = gameObject.GetComponents<EnvironmentData>();
    }

    public void updateCurrentBeat(int beatID) //called from Game when the beat ID changes
    {
        //cycles each data piece in the beatData list
        for (int i = 0; i < beatData.Length; i++)
        {
            //checks if the ID switched to has any action associated with it
            if(beatData[i].beatExecutedOn == beatID)
            {
                //if it does, execute said action (unityEvent)
                

                beatData[i].beatEvents.Invoke();
            }
        }
    }

    public void Update()
    {
        if(!solved)
        {
            //O2 puzzle update

            //stores current number of correct buttons down in puzzle
            int correct = 0;

            //each update, check if any buttons that are meant to be down are down
            for (int i = 0; i < O2Buttons.Count; i++)
            {
                //if the value (true / false) stored in the array corrospond with the button data (e.g. Yellow is flagged as needing to be down, and it is down)
                if(O2Buttons[i].pressed == downVsUp[i])
                {
                    //increase the number of "correct" buttons by 1
                    correct += 1;
                }
            }

            if(correct == O2Buttons.Count) //if all buttons are in the right positions, open the o2 door
            {
                O2PuzzleComplete.Invoke();
                solved = true;
            }
            
        }
        
    }

    public void DebugExecution(int beat)
    {
        Debug.LogWarning(beat);
    }

}
