﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public bool displayed = false;
    private CanvasGroup canvas;

    [SerializeField]
    private PlayerMovement _playerMovement;

    [SerializeField]
    private NoteMenu noteMenu;

    [SerializeField]
    private Canvas crosshair;

    private void Start()
    {
        canvas = gameObject.GetComponent<CanvasGroup>();
        canvas.alpha = 0;
        canvas.interactable = false;
        canvas.blocksRaycasts = false;
        displayed = false;


    }
   

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(noteMenu.displayed == true)
            {
                noteMenu.setDisplay(false);
            }
            else
            {
                displayed = !displayed;

                canvas.alpha = displayed? 1 : 0;
                canvas.interactable = displayed;
                canvas.blocksRaycasts = displayed;
                Cursor.lockState = displayed ? CursorLockMode.Confined : CursorLockMode.Locked;
                Cursor.visible = displayed;

                _playerMovement.enabled = !displayed;

                Time.timeScale = displayed ? 0 : 1;

                crosshair.enabled = !displayed;
            }
            
        }
    }

    public void Resume()
    {
        displayed = false;

        
        canvas.alpha = displayed ? 1 : 0;
        canvas.interactable = displayed;

        Cursor.lockState = displayed ? CursorLockMode.Confined : CursorLockMode.Locked;
        Cursor.visible = displayed;

        _playerMovement.enabled = !displayed;

        Time.timeScale = displayed ? 0 : 1;

        crosshair.enabled = displayed ? false : true;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
