﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

//Allows an item to be "interacted with" in any way defined in the Unity Event object
public class Interactable : MonoBehaviour
{
    //events to execute on "interaction"
    [SerializeField]
    private UnityEvent _onInteract;

    private AudioManager _audioManager;

    //toggles whether or not item can be used at this time
    [SerializeField]
    private bool _interactable = true;

    //get / set interactable value
    public bool interactable { get { return _interactable; } set { _interactable = value; } }

    private void Start()
    {
        _audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
    }

    //when "intereacted with", execute all methods inside the Unity Event
    public void interact()
    {
        if(_onInteract != null && _interactable)
        {
            _onInteract.Invoke();
        }

        if(!_interactable)
        {
            _audioManager.Play("Denied");
        }
    }
}
