﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//HAS BEEN REPLACED BY THE INTERACTABLE SCRIPT AS OF 11.12.2020
//if you just read it you'll understand why, it's not in the least bit effectively written

public class DoorButton : MonoBehaviour
{
    [SerializeField]
    public List<Door> doorsToInteract; //lists doors to open / close

    [SerializeField]
    private bool _locked = false; //sets door "locked" status via inspector

    private bool opened = false; //door state 

    public bool locked { set { _locked = value; }  }

    public void openDoor()
    {
        if(!_locked) //when called, if the door isn't locked, cycle each door in the list & open it
        {
            foreach(Door door in doorsToInteract)
            {
                door.interact();
            }
        }

        opened = !opened; //opened is switched
    }


}
