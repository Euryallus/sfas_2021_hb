﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//attached to objects that can be picked up & moved (REQUIRES A RIGIDBODY FOR THIS TO WORK)
public class movableObject : MonoBehaviour
{
    //bool value (toggled if player is holding the item)
    [SerializeField]
    private bool _isHeld;

    //saves "hand" position of player
    private Transform target;

    //stores ref. to Rigidbody component
    [SerializeField]
    private Rigidbody rb;


    void Start()
    {
        _isHeld = false;
        rb = gameObject.GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        //if the player is currently holding the object, move towards the players hand position
        if(_isHeld) 
        {
            transform.position = Vector3.LerpUnclamped(transform.position, target.position, 5 * Time.deltaTime);
        }
    }

    //sets target position to players hand, turns off grav & sets _isHeld to true
    public void pickUp(Transform hand)
    {
        target = hand;
        rb.useGravity = false;
        _isHeld = true;
    }

    //sets item down where it is and re-enables grav.
    public void dropObject(Vector3 direction)
    {
        _isHeld = false;
        rb.useGravity = true;
    }

    //throws object in the direction the player is facing, re-enables grav etc.
    public void throwObject(Vector3 direction)
    {
        _isHeld = false;
        rb.useGravity = true;
        rb.AddForce(direction.normalized * 600);
    }

    //returns whether item is held or not
    public bool getPickedUp()
    {
        return _isHeld;
    }
}
