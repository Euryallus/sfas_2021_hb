﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Transform _destination;

    [SerializeField]
    private Game _gameController; //Game object stored to retrieve beat data

    [SerializeField]
    private GameObject _playerCamera; //player camera (stataic)
    [SerializeField]
    private TextDisplay _textDisplay; //screen display element

    //speed at which camera lerps between fixed pos (on player) to terminal view
    [SerializeField]
    private float transitionSpeed = 5f;

    Vector3 originalPos;

    //camera used to project terminal screen texture
    [SerializeField]
    private GameObject screenCamera; 

    //possible "states" camera can be in (either moving from playerCam to terminalCam, focused on terminal, or "idle"
    public enum state
    {
        moving,
        focused,
        stationary
    }

    public state currentState; //stores current state

    private void Start()
    {
        //sets state to stationary as default
        currentState = state.stationary;
        //sets screensaver to active
        originalPos = screenCamera.transform.position;

        //screenCamera.transform.position = originalPos + new Vector3(0, 0, 1);
        
    }

    // Update is called once per frame
    void Update()
    {
        //if camera is moving, liniarly interpolate between current pos and destination pos (terminal camera point) at transitionSpeed
        if(currentState == state.moving) 
        {
            transform.position = Vector3.Lerp(transform.position, _destination.position, transitionSpeed * Time.deltaTime);

            transform.rotation = Quaternion.Lerp(transform.rotation, _destination.rotation, transitionSpeed * Time.deltaTime);

            //if camera is close enough, just set position of the camera to the terminal point & set state to focused
            if(Vector3.Distance(transform.position, _destination.position) < 0.01f) 
            {
                currentState = state.focused;
            }
        }

        //toggles state to "stationary" again, switches to player camera again
        if(Input.GetKeyDown(KeyCode.E) && currentState == state.focused)
        {
            currentState = state.stationary;
            _playerCamera.SetActive(true);

            //stops text being printed to terminal screen
            _gameController.Stop();

            //disables self
            gameObject.SetActive(false);
            
            //sets terminal screen cam to pos where cube is visible & begins screensaver again
            screenCamera.transform.position = originalPos + new Vector3(0, 0, 1);

        }
    }

    //called when player goes to use terminal (kick starts transition sequence)
    public void _playerToFocus(Transform initialPos, Transform desinationPos) 
    {
        //disables screensaver

        //stores destination as point passed as parameter
        _destination = desinationPos;

        //sets pos. of transition camera to mimic player camera
        transform.position = initialPos.position;
        transform.rotation = initialPos.rotation;

        //sets state to transitional state (moving)
        currentState = state.moving;

        screenCamera.transform.position = originalPos;
    }
}
