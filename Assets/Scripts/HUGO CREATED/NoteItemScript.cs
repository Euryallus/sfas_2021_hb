﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
//data stored by each note button in the menu
public class NoteItemScript : MonoBehaviour
{
    //stores note data
    [SerializeField]
    private NoteData _noteData;

    //stores ref. to description text box
    [SerializeField]
    private TMP_Text description;

    //set / get note data
    public NoteData noteData { get { return _noteData; } set { _noteData = value; } }
     
    //sets description text to associated note data 
    public void setDescription()
    {
        description.text = noteData.contents;
    }


}
