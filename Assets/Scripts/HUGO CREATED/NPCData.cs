﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character Data", menuName = "Characters / Create new character data", order = 1)] 
//created to allow the positibility of different "characters" to interact w/ player via the terminals
public class NPCData : ScriptableObject
{


    //allows dev to assign each character a unique colour text & talk speed
    [SerializeField]
    private Color _characterColour;

    [SerializeField]
    private float _talkSpeed;

    //getters / setters for character data
    public Color characterColour { get { return _characterColour; } }
    public float talkSpeed { get { return _talkSpeed; } }

}
