﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using UnityEngine.EventSystems;

using TMPro;

//controls & manages notes menu
public class NoteMenu : MonoBehaviour
{
    //list of notes collected throughout the scene
    [SerializeField]
    private List<NoteData> _collectedNotes;

    //list of all note buttons created in menu
    [SerializeField]
    public List<GameObject> _noteButtons;

    //ref. to playerMovement script (prevents movement when notes menu is up)
    [SerializeField]
    private PlayerMovement _playerMovement;

    //saves ref. to canvas group to control alpha
    [SerializeField]
    private CanvasGroup noteCanvas;

    //bool saves whether menu is shown or not
    public bool displayed = false;

    //stores "template" note button
    [SerializeField]
    private GameObject noteOptionButtonPrefab;

    //stores reference to description text
    [SerializeField]
    private TMP_Text _description;

    //stores ref. to the button container (uses a scroll & render mask)
    [SerializeField]
    private GameObject noteButtonContainer;

    [SerializeField]
    private Animator notePopUpAnimator;
    [SerializeField]
    private TMP_Text noteTitle;



    private void Start()
    {
        //initially sets menu to hidden & toggles "interactable" to off
        noteCanvas.alpha = 0;
        noteCanvas.interactable = false;
        noteCanvas.blocksRaycasts = false;
    }

    void Update()
    {
        //detects if the "notes" key is pressed at all
        if (Input.GetKeyDown(KeyCode.N) || Input.GetKeyDown(KeyCode.Tab))
        {
            //flips displayed value
            displayed = !displayed;

            if(displayed)
            {
                _playerMovement.StopCoroutine("footsteps");
            }

            setDisplay(displayed);
        }

    }

    public void setDisplay(bool displayState)
    {
        //switches between visible & invisible (turns menu on / off)
        displayed = displayState;
        UnityEngine.Cursor.lockState = displayed ? CursorLockMode.Confined : CursorLockMode.Locked;
        UnityEngine.Cursor.visible = displayed;

        noteCanvas.alpha = displayed ? 1 : 0;
        noteCanvas.blocksRaycasts = displayed;
        noteCanvas.interactable = displayed;
        _playerMovement.enabled = !displayed;

        if(displayed)
        {
            _playerMovement.openMenu();
        }

    }

    //called when a note is collected
    public void addNote(NoteData note)
    {
        //adds the note passed to list of collected notes 
        _collectedNotes.Add(note);

        if (_noteButtons.Count == 0)
        {
            //if there are no other note buttons in the menu, create a new one
            _noteButtons.Add(Instantiate(noteOptionButtonPrefab, noteButtonContainer.transform));
            int index = 0;

            //sets the text component of the button to the title of the note collected
            _noteButtons[index].transform.GetChild(1).GetComponent<TMP_Text>().text = note.title;
            //sets button to active (just in case)
            _noteButtons[index].SetActive(true);
            //saves the noteData to the button using the NoteItemScript component
            _noteButtons[index].GetComponent<NoteItemScript>().noteData = note;

            //sets position of the button to (0, -50) inside the button container
            _noteButtons[index].GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -50);

        }
        else
        {
            //the same thing as before...
            _noteButtons.Add(Instantiate(noteOptionButtonPrefab, noteButtonContainer.transform));

            int index = _noteButtons.Count - 1;
            _noteButtons[index].SetActive(true);

            _noteButtons[index].transform.GetChild(1).GetComponent<TMP_Text>().text = note.title;

            _noteButtons[index].GetComponent<NoteItemScript>().noteData = note;

            //... except the position is now adjusted to be below whichever button is above it 
            _noteButtons[index].GetComponent<RectTransform>().anchoredPosition = _noteButtons[_noteButtons.Count - 2].GetComponent<RectTransform>().anchoredPosition + new Vector2(0, -100);

        }
        //resizes the button container (allows endless notes, unlimited scaling!
        noteButtonContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(noteButtonContainer.GetComponent<RectTransform>().sizeDelta.x, noteButtonContainer.GetComponent<RectTransform>().sizeDelta.y + (_noteButtons[0].GetComponent<RectTransform>().sizeDelta.y + 50));
        
        noteTitle.text = note.title;
        notePopUpAnimator.SetBool("displayPopUp", true);

        StartCoroutine("resetAnim");

    }

    public void displayContents()
    {
        //scream(); //don't actually do this

        //sets description text to the description stored in the button data
        _description.text = gameObject.GetComponent<NoteItemScript>().noteData.contents;
    }

    public void scream()
    {
        //this was a test, can you tell I was having a hard time implementing stuff?
        Debug.Log("AAAAAAAAAAAAAAAAAAAA");
    }

    public IEnumerator resetAnim()
    {
        yield return new WaitForSeconds(2.5f);
        notePopUpAnimator.SetBool("displayPopUp", false);
    }


    
}
